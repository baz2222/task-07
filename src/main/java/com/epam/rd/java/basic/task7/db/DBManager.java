package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.FileInputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class DBManager {
    private static Connection con;
    private static DBManager instance = null;

    public static synchronized DBManager getInstance() {
        if (instance == null)
            instance = new DBManager();
        return instance;
    }

    private DBManager() {
        String connectionURL;
        try {
            Properties properties = new Properties();
            FileInputStream fs = new FileInputStream("app.properties");
            properties.load(fs);
            fs.close();
            connectionURL = properties.getProperty("connection.url");
            con = DriverManager.getConnection(connectionURL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        ResultSet rs;
        try {
            if (con != null) {
                Statement s = con.createStatement();
                rs = s.executeQuery("SELECT * FROM users");
                User user;
                while (rs.next()) {
                    user = new User();
                    user.setId(rs.getInt("id"));
                    user.setLogin(rs.getString("login"));
                    users.add(user);
                }
                s.close();
            }
        } catch (Exception e) {
            throw new DBException(e.getMessage(), e.getCause());
        }
        return users;
    }

    public boolean insertUser(User user) throws DBException {
        if (!user.getLogin().matches("\\w+") || user.getLogin().length() > 10)
            throw new IllegalArgumentException("wrong login format");
        try (PreparedStatement statement = con.prepareStatement("INSERT INTO users(login) VALUES(?)", Statement.RETURN_GENERATED_KEYS)) {
            if (con != null) {
                statement.setString(1, user.getLogin());
                statement.executeUpdate();
                ResultSet rs = statement.getGeneratedKeys();
                rs.next();
                user.setId(rs.getInt(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e.getCause());
        }
        return true;
    }

    public boolean deleteUsers(User... users) throws DBException {
        try {
            if (con != null) {
                Statement s = con.createStatement();
                for (User user : users)
                    s.execute("DELETE FROM users WHERE id = " + user.getId());
                s.close();
                return true;
            }
        } catch (Exception e) {
            throw new DBException(e.getMessage(), e.getCause());
        }
        return false;
    }

    public User getUser(String login) throws DBException {
        User user = new User();
        ResultSet rs;
        try {
            if (con != null) {
                Statement s = con.createStatement();
                rs = s.executeQuery("SELECT id, login FROM users WHERE login = '" + login + "'");
                rs.next();
                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
                s.close();
            }
        } catch (Exception e) {
            throw new DBException(e.getMessage(), e.getCause());
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = new Team();
        ResultSet rs;
        try {
            if (con != null) {
                Statement s = con.createStatement();
                rs = s.executeQuery("SELECT id, name FROM teams WHERE name = '" + name + "'");
                rs.next();
                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));
                s.close();
            }
        } catch (Exception e) {
            throw new DBException(e.getMessage(), e.getCause());
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        ResultSet rs;
        try {
            if (con != null) {
                Statement s = con.createStatement();
                rs = s.executeQuery("SELECT * FROM teams");
                Team team;
                while (rs.next()) {
                    team = new Team();
                    team.setId(rs.getInt("id"));
                    team.setName(rs.getString("name"));
                    teams.add(team);
                }
                s.close();
            }
        } catch (Exception e) {
            throw new DBException(e.getMessage(), e.getCause());
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        if (!team.getName().matches("\\w+") || team.getName().length() > 10)
            throw new IllegalArgumentException("wrong name format");
        try (PreparedStatement statement = con.prepareStatement("INSERT INTO teams(name) VALUES(?)", Statement.RETURN_GENERATED_KEYS)) {
            if (con != null) {
                statement.setString(1, team.getName());
                statement.executeUpdate();
                ResultSet rs = statement.getGeneratedKeys();
                rs.next();
                team.setId(rs.getInt(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e.getCause());
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        List<Team> tms = Arrays.asList(teams);
        StringBuilder request = new StringBuilder();
        request.append("INSERT INTO users_teams(user_id, team_id) VALUES ");
        for (int i = 0; i < tms.size(); i++) {
            request.append("(" + user.getId() + ", " + tms.get(i).getId() + ")");
            if (i < tms.size() - 1)
                request.append(", ");
        }//for
        try {
            if (con != null) {
                Statement s = con.createStatement();
                s.execute(request.toString());
                s.close();
                return true;
            }
        } catch (Exception e) {
            throw new DBException(e.getMessage(), e.getCause());
        }
        return false;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList<>();
        List<Integer> ids = new ArrayList<>();
        Team team;
        int id;
        ResultSet rs;
        try {
            if (con != null) {
                Statement s = con.createStatement();
                rs = s.executeQuery("SELECT * FROM users_teams WHERE user_id =" + user.getId());
                while (rs.next()) {
                    ids.add(rs.getInt("team_id"));
                }//while
                rs = s.executeQuery("SELECT * FROM teams");
                while (rs.next()) {
                    id = rs.getInt("id");
                    if (ids.contains(id)) {
                        team = new Team();
                        team.setId(id);
                        team.setName(rs.getString("name"));
                        teams.add(team);
                    }
                }//while
                s.close();
            }//if
        } catch (Exception e) {
            throw new DBException(e.getMessage(), e.getCause());
        }
        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        try {
            if (con != null) {
                Statement s = con.createStatement();
                s.execute("DELETE FROM teams WHERE id = " + team.getId());
                s.close();
                return true;
            }
        } catch (Exception e) {
            throw new DBException(e.getMessage(), e.getCause());
        }
        return false;
    }

    public boolean updateTeam(Team team) throws DBException {
        try {
            if (con != null) {
                Statement s = con.createStatement();
                s.execute("UPDATE teams SET name = '" + team.getName() + "' WHERE id =" + team.getId());
                s.close();
                return true;
            }
        } catch (Exception e) {
            throw new DBException(e.getMessage(), e.getCause());
        }
        return false;
    }

}
