package com.epam.rd.java.basic.task7.db;

public class DBException extends Exception {
	private final String message;
	private final Throwable cause;

	public DBException(String message, Throwable cause) {
		this.message = message;
		this.cause = cause;
	}
	@Override
	public String getMessage(){
		return message;
	}
	@Override
	public Throwable getCause(){
		return cause;
	}
}
